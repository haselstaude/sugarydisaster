import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class ColliderTest extends JFrame implements KeyListener {

	// application
	private static final long serialVersionUID = 1L;
	private static final int frameWidth = 1280;
	private static final int frameHeight = 720;
	
	// general
	public boolean running = true;
	public long lastNano;
	public byte controls_move = 0;			// Up = 1, Right = 2, Down = 3, Left = 4
	
	// rendering
	protected Graphics2D rend_g;			// Graphics of real canvas
	protected BufferedImage rend_bb;		// Backbuffer
	protected Graphics2D rend_bbg;			// Graphics of Backbuffer
	protected Insets rend_insets;			// Insets of window
	
	// controllable
	protected float cont_x, cont_y;
	protected static final int CONT_SIZE = 50;
	protected int cont_vel = 200;
	
	// obstacle
	protected int obst_x = 500, obst_y = 300;
	protected static final int OBST_WIDTH = 100;
	protected static final int OBST_HEIGHT = 200;
	
	public static void main(String[] args) {
		ColliderTest game = new ColliderTest();
		game.lastNano = System.nanoTime();
		game.run();
		System.exit(0);
	}
	
	
	/**
	 * Sets up and runs game
	 */
	protected void run() {
		this.initWindow();
		
		while (this.running) {
			// delta time
			long deltaTimeInNanos = System.nanoTime() - this.lastNano;
			float deltaTimeInSeconds = deltaTimeInNanos / 1000000000f;
			this.lastNano = System.nanoTime();
			
			// update and render
			this.update(deltaTimeInSeconds);
			this.collider();
			this.render();
		}
		setVisible(false);
	}
	
	/**
	 * Setting up the window
	 */
	protected void initWindow() {
		setTitle("Collider Test");
		setSize(ColliderTest.frameWidth, ColliderTest.frameHeight);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		addKeyListener(this);
		
		this.rend_insets = getInsets();
		setSize(
			this.rend_insets.left + ColliderTest.frameWidth + this.rend_insets.right, 
			this.rend_insets.top + ColliderTest.frameHeight + this.rend_insets.bottom
		);
		
		this.rend_g = (Graphics2D) getGraphics();
		this.rend_bb = new BufferedImage(ColliderTest.frameWidth, ColliderTest.frameHeight, BufferedImage.TYPE_INT_RGB);
		this.rend_bbg = (Graphics2D) this.rend_bb.getGraphics();
	}
	
	/**
	 * Physics loop
	 * @param deltaTime Time since last frame [s]
	 */
	protected void update(float deltaTime) {
		// check input
		switch(controls_move) {
			case 1: {
				cont_y -= cont_vel * deltaTime;
				break;
			}
			case 2: {
				cont_x += cont_vel * deltaTime;
				break;
			}
			case 3: {
				cont_y += cont_vel * deltaTime;
				break;
			}
			case 4: {
				cont_x -= cont_vel * deltaTime;
				break;
			}
			default: {}
		}
	}
	
	/**
	 *  Collider
	 */
	protected void collider() {
		// Check if inside x
		if(cont_x + CONT_SIZE > obst_x && cont_x < obst_x + OBST_WIDTH) {
			// collision x
			// check if inside y
			if(cont_y + CONT_SIZE > obst_y && cont_y < obst_y + OBST_HEIGHT) {
				// collision
				float correction_x = 0f;
				float correction_y = 0f;
				switch(controls_move) {
					case 1: {
						correction_y = obst_y + OBST_HEIGHT - cont_y;
						break;
					}
					case 4: {
						correction_x = obst_x + OBST_WIDTH - cont_x;
						break;
					}
					case 2: {
						correction_x = (-1f)* (cont_x + CONT_SIZE - obst_x);
						break;
					}
					case 3: {
						correction_y = (-1f) * (cont_y + CONT_SIZE - obst_y);
						break;
					}
					default: {}
				}
				cont_x += correction_x;
				cont_y += correction_y;
			}
		}
	}
	
	/**
	 * Render loop
	 */
	protected void render() {
		// clear backbuffer
		this.rend_bbg.clearRect(0, 0, ColliderTest.frameWidth, ColliderTest.frameHeight);
		
		// set background
		this.rend_bbg.setColor(Color.WHITE);
		this.rend_bbg.fillRect(0, 0, ColliderTest.frameWidth, ColliderTest.frameHeight);
		
		// render controllable
		this.rend_bbg.setColor(Color.BLACK);
		this.rend_bbg.fillRect((int)cont_x, (int)cont_y, CONT_SIZE, CONT_SIZE);
		
		// render obstacle
		this.rend_bbg.setColor(Color.RED);
		this.rend_bbg.fillRect(obst_x, obst_y, OBST_WIDTH, OBST_HEIGHT);
		
		// buffer applying
		this.rend_g.drawImage(this.rend_bb, this.rend_insets.left, this.rend_insets.top, this);
	}

	@Override
	public void keyPressed(KeyEvent event) {
		// moving
		if (event.getKeyCode() == KeyEvent.VK_LEFT) {
			this.controls_move = 4;
		} else if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
			this.controls_move = 2;
		} else if (event.getKeyCode() == KeyEvent.VK_UP) {
			this.controls_move = 1;
		} else if (event.getKeyCode() == KeyEvent.VK_DOWN) {
			this.controls_move = 3;
		}
	}

	@Override
	public void keyReleased(KeyEvent event) { 
		// moving
		if (event.getKeyCode() == KeyEvent.VK_LEFT || event.getKeyCode() == KeyEvent.VK_RIGHT || event.getKeyCode() == KeyEvent.VK_UP || event.getKeyCode() == KeyEvent.VK_DOWN) {
			this.controls_move = 0;
		}
	}

	@Override
	public void keyTyped(KeyEvent event) { }

}